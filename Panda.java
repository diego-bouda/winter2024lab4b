public class Panda {

    private int age;
    private String species;
    private boolean hungry;


    public Panda(int age, String species, boolean hungry) {
        this.age = age;
        this.species = species;
        this.hungry = hungry;
    }

    public void eat() {
        if (hungry)
            System.out.println("This " + species + " Panda is really hungry, he's eating so much bamboo.");
        else
            System.out.println("This " + species + " Panda isn't eating anything, he must not be hungry.");
    }

    public void roll() {
        if (age <= 5)
            System.out.println("Wow! This " + species + " Panda rolls around so fast, he must be young!");
        else
            System.out.println("Oh no... This " + species + " Panda rolls around slowly, poor old panda :(");
    }
    public int getAge() {
        return this.age;
    }
    public String getSpecies() {
        return this.species;
    }
    public Boolean getHungry() {
        return this.hungry;
    }
    public void setAge(int newAge) {
        this.age = newAge;
    }
}
