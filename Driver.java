import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Panda[] embarrassment = new Panda[3];
        
        for (int i = 0; i < embarrassment.length; i++) {
            System.out.println("Add information about the panda:");

            System.out.println("How old is your panda?");
            int age = Integer.parseInt(reader.nextLine());

            System.out.println("What species does this panda belong to?");
            String species = reader.nextLine();

            System.out.println("Is your panda hungry? yes/no");
            String hungryAnswer = reader.nextLine();

            // Checking the user's String input to then evaluate it as a boolean
            boolean hungry = false;
            if (hungryAnswer.equals("yes"))
                hungry = true;

            embarrassment[i] = new Panda(age, species, hungry);

        }
		
        System.out.println(embarrassment[embarrassment.length -1 ].getAge());
        System.out.println(embarrassment[embarrassment.length -1 ].getSpecies());
        System.out.println(embarrassment[embarrassment.length -1 ].getHungry());
        embarrassment[embarrassment.length -1 ].setAge(777);
        System.out.println(embarrassment[embarrassment.length -1 ].getAge());
        System.out.println(embarrassment[embarrassment.length -1 ].getSpecies());
        System.out.println(embarrassment[embarrassment.length -1 ].getHungry());
       

        reader.close();
    }
}
